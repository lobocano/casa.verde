
/**
 * Module dependencies.
 */
var gpio = require('gpio');

var express = require('express')
    , bodyParser = require('body-parser')
    , routes = require('./routes')
    , user = require('./routes/user')
    , pi = require('./routes/pi')
    , http = require('http')
    , path = require('path');

 var app = express();
// all environments
app.set('port', process.env.PORT || 3001);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
//app.use(express.favicon());
app.use(require('morgan')('dev'));
app.use(bodyParser());

//app.use(express.json());
//app.use(express.urlencoded());
app.use(require('method-override')());
//app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
  //app.use(express.errorHandler());
}

app.get('/', routes.index);
app.get('/users', user.list);
app.get('/status', pi.getStatus);
app.post('/status', pi.setStatus);
app.post('/gpio', pi.setPin);
app.get('/themp-hist', pi.getThempHist);
app.get('/config', pi.getConf);
app.post('/config', pi.patchConf);
app.get('/power', pi.getPowerHist);


http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
    //setpin(27, 0);
});

function setpin(pin, val){
    console.log('setPin: %d %d', pin, val);
    var gpioxx = gpio.export(pin, {
        direction: 'out',
        interval: 200,
        ready: function() {
            console.log('ready');
            gpioxx.set(val, function() {
                console.log('value: %d',gpioxx.value);
                gpioxx.unexport();
            });
        }
    });

}
