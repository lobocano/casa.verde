/**
 * Created by pablo on 4/24/14.
 */
var gpio = require('gpio');
var gpioxx = gpio.export(27, {
    direction: 'out',
    interval: 200,
    ready: function() {
        console.log('ready');
        gpioxx.set(1, function() {
            console.log('value: ',gpioxx.value);    // should log 1
            gpioxx.unexport();
        });
    }
});
