/**
 * Created by pablo on 14.10.14.
 */
var piutils = require('piutils');
var logger = piutils.logger;
var arango = require('arango'),
    db = arango.Connection('http://localhost:8529');
var stateFile = piutils.files["state"];
logger.debug('measure started', stateFile);
piutils.readCounters(function(err,data){
    if(err) logger.error(err);
    else {
        logger.debug(data);
        data.ticks[0].count = 0;
        piutils.writeCounters(data, function(err){
            if(err)logger.error(err);
            else logger.debug('done');
        })
    }
});
