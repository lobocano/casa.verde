//var gpio = require('gpio');
var Gpio = require('onoff').Gpio;
var jf = require('jsonfile');
var arango = require('arango');
var db = arango.Connection("http://localhost:8529");
var util = require('util');
var piutils = require('piutils');
var logger = piutils.logger;

var stateFile = "/home/pi/casaverde/state.json";
var confFile = "/home/pi/casaverde/casa.conf";

exports.getStatus = function(req,res){
    piutils.composeState(function(err,st){
        if(err)res.json({error:err});
        else res.json(st);
    });
};

exports.getThempHist = function(req,res){
    jf.readFile(confFile, function(err, conf) {
        if(err){
            console.log("error read conf");
            res.json({status: 'error'});
        }
        else {
            console.log("get data");
                db.query.exec("FOR t in ThempHist FILTER t.sensorId == @id SORT t.fecha  RETURN {fecha: t.fecha, themp: t.themp, kitchen: t.kitchen, cellar: t.cellar, outdoor: t.outdoor, ticks0: t.ticks0, ticks1: t.ticks1}",{id:conf.rooms[0].sensorId},function(err,ret){
                    res.json(ret.result);


                });
             /*db.query.exec("LET cnt = (FOR t in ThempHist FILTER t.sensorId == @id RETURN 1) RETURN LENGTH(cnt)",{id:conf.rooms[0].sensorId},function(err,ret){
                console.log("err(%s):", err, ret);
                var count = ret.result[0];
                db.query.exec("FOR t in ThempHist FILTER t.sensorId == @id SORT t.fecha LIMIT @offs, 60 RETURN {fecha: t.fecha, themp: t.themp, outdoor: t.outdoor}",{id:conf.rooms[0].sensorId, offs: count-60},function(err,ret){
                    res.json(ret.result);


                });
            });*/
        }
    });

};

exports.getPowerHist = function(req,res){
    db.query.exec("FOR p in AvgPower SORT p.fecha  RETURN {fecha: p.fecha, power: p.power}",function(err,ret){
        res.json(ret.result);


    });
};

exports.setStatus = function(req,res){
    //ToDo: replace with real use of Raspberry PI GPIO

    console.log('setStatus: %j', req.body);
    res.json({status: 'success'});
};

exports.setPin = function(req, res){
    console.log('setPin: %d %d', req.body.pin, req.body.val);
    var gpioxx = new Gpio(req.body.pin, 'out');
    gpioxx.writeSync(+ req.body.val);
    res.json({status: 'success', pin: req.body.pin, val: req.body.val});
    /*var gpioxx = gpio.export(req.body.pin, {
        direction: 'out',
        interval: 100,
        ready: function() {
            console.log('ready');
            gpioxx.set(req.body.val, function() {
                console.log('value: %d',gpioxx.value);    // should log 1
                gpioxx.unexport();
                console.log('unexport');
                res.json({status: 'success'});
             });
        }
    });*/

    //res.json({status: 'success'});
    /*var gpioxx = gpio.export(req.body.pin, {
        direction: 'out',

        interval: 200,

        ready: function() {
            console.log('ready');
            gpioxx.set(req.body.val, function() {
                console.log(req.body.val);
                gpioxx.unexport();
            });
        }
    });*/
};

exports.getConf = function(req,res){
    jf.readFile(confFile, function(err, conf) {
        if(err){
            console.log("error read conf");
            res.json({status: 'error'});
        }
        else {
            res.json(conf);
        }
    });
};

exports.patchConf = function(req,res){
    logger.debug('patchConf');
    piutils.readConfig(function(err,_conf){
       if(err) res.json({error:err});
        else{
           //logger.debug('body', util.inspect(req.body));
           var conf = _conf, key, val;
           try{
               var isArray = util.isArray(req.body);
               logger.debug('isArray', isArray);
               if(isArray){

                   req.body.forEach(function(item){
                       //logger.debug(item);
                       key = item.prop;
                       val = item.val;

                       logger.debug('key: %s newWal: %s, oldVal: %s',key, val, conf[key]);
                       //if(key != 'rooms')
                       if(val) conf[key] = val;
                   })
               }
               else{
                   key = req.body.prop;
                   val = req.body.val;
                   logger.debug('key: %s newWal: %s, oldVal: %s',key, val, conf[key]);
                   conf[key] = val;
               }
               piutils.writeConfig(conf, function(err) {
                   if (err) res.json({error: err});
                   else {
                       res.json({status: 'success'})
                   }
               });
           }
           catch(e){
               logger.error(e.message);
               res.json({error: e.message})
           }
           //logger.debug(conf);


           //console.log(conf);
           //res.json({status:'success'});

       }

    });
};

