/**
 * Created by pablo on 28.05.15.
 */

'use strict';

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj['default'] = obj; return newObj; } }

//var thermostat = require('./thermostat');

var _thermostat = require('./thermostat');

var thermostat = _interopRequireWildcard(_thermostat);

var constants = require('./constants');
var argv = require('optimist').argv;
var moment = require('moment');

var jf = require('jsonfile'),
    fs = require('fs');
var readline = require('readline');
var term = require('terminal-kit').terminal;

var rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});
var minutes = 1,
    timer = moment().add(minutes, 'm');
var ts,
    powerMode,
    pFull,
    bellCount = 5,
    bellInterval,
    programCounter = 0,
    step;

var fName = argv.f;
if (!fName || fName == '') {
    console.log('Config file not specified!');
    process.exit(103);
}

jf.readFile(fName, function (err, cnf) {
    console.dir(cnf);
    //process.exit(103);
    powerMode = cnf.powerMode; //(argv.p != undefined && argv.p != "");
    pFull = cnf.pFull;
    ts = new thermostat.Thermostat(cnf.name, cnf.offset, cnf.gpioNum, cnf.id, 20, terminal, cnf.loopback);
    if (cnf.program) {
        ts.program = cnf.program;
        step = ts.program[programCounter];
        timer = moment().add(step.m, 'm');
        setStep();
    } else {
        ts.Tmax = 35;
        ts.Power = 5;
        timer = moment().add(cnf.timer, 'm');
    }
    /*if(powerMode){
        //ts = new thermostat.HeatDevice('queso', 0, 17);
        //ts = new thermostat.Thermostat('queso', 0, 17, '28-000006700222', 20, terminal, false);
        ts = new thermostat.Thermostat('queso', 0, 17, '28-000006879f5f', 20, terminal, false);
    }
    else{
        ts = new thermostat.Thermostat('queso', 0, 17, '28-000006879f5f', 20, terminal, true);
        ts.Tmax = 35;
      }*/
    term.eraseDisplay();
    //minutes = cnf.timer;

    ts.isOn = true;
    rl.on('line', lineHandler);
    rl.on('SIGINT', sigint);
});
/*var fumo = {
    name:'sam',
    offset:0,
    gpioNum: 17,
    powerMode:true,
    id:'28-000006700222',
    loopback:false
};

jf.writeFile('sam.json', fumo, function (err) {
    //console.error(err)
});*/

function setPower(power) {
    ts.Power = power;
}

function lineHandler(line) {
    term.moveTo(1, 3).eraseLine();
    var match = /^m([0-9]+)$/.exec(line);
    if (match) {
        term.green(match[1]);
        minutes = match[1];
        timer = moment().add(minutes, 'm');
        ts.isOn = true;
    } else if (line == 'n') {
        // next step
        if (programCounter < ts.program.length - 1) {
            programCounter++;
            setStep();
        }
    } else {
        var p = parseFloat(line);
        //console.log('t max: ',p);
        //setPower(p);
        //
        // 20
        //ts.setPower(p);
        if (powerMode) {

            ts.Power = p * 100 / pFull;
            //term.green("P: %d ",p);
        } else {
            ts.Tmax = p;
            //term.green("t: %d ",p);
        }
    }
}

function sigint() {
    ts.isOn = false;
    rl.close();
    process.exit(103);
}

function terminal(tmax, current, delta, power) {
    var elapsed = timer - moment();
    var pWt = power * pFull / 100;
    //var ms = +elapsed;
    term.moveTo(1, 1);
    term.eraseLine();
    if (ts.program) step = ts.program[programCounter];
    if (!powerMode) term.bold().green('%d %f %s  P(W): %d', tmax, current, delta.toFixed(1), pWt);else term.bold().yellow('t(C): %f  P(W): %d', current, pWt);
    if (elapsed >= 0) term.nextLine().eraseLine().green('Step %d ETA: %s %s', programCounter + 1, moment(elapsed).utc().format('HH:mm:ss'), step.n);else {
        term.nextLine().eraseLine().bold().red('00:00:00');
        if (ts.program) {
            programCounter++;
            if (programCounter >= ts.program.length) {
                ts.isOn = false;
            } else {
                //var step = ts.program[programCounter];

                setStep();
            }
        } else {
            ts.isOn = false;
        }

        bellCount = 5;
        //term.bell();
        bellInterval = setInterval(function () {
            //console.log(bellCount);
            term.bell();
            bellCount--;
            if (bellCount <= 0) clearInterval(bellInterval);
        }, 1000);
    }
    term.nextLine();
}
function setStep() {
    if (ts.program) step = ts.program[programCounter];
    timer = moment().add(step.m, 'm');
    if (step.t) {
        ts.Tmax = step.t;
    } else if (step.p) {
        ts.Power = step.p * 100 / pFull;
    }
}
//var queso = new thermostat.HeatDevice('queso', 0);
//queso.Pover = 50;

//queso.isOn = true;
//console.log(queso.Pover);
//ts.isOn = false;

//# sourceMappingURL=queso.js.map