/**
 * Created by pablo on 14.10.14.
 * version 0.0.2
 */
'use strict';

var constants = require('./constants');
var winston = require('winston');
var moment = require('moment');
var util = require('util');
var nodemailer = require('nodemailer'),
    SMSru = require('sms_ru');
var jf = require('jsonfile'),
    fs = require('fs');
var ws = require('nodejs-websocket'),
    wsConn;
var files = constants.files;
var logger = new winston.Logger({
    transports: [new winston.transports.Console({ level: 'debug' }),
    //new (winston.transports.File)({ filename: files["log"], level: 'debug',json: false, maxsize: 1024, maxFiles:5 }),
    new winston.transports.File({ filename: files['log'], level: 'debug', json: false, maxsize: 65536, maxFiles: 5 })]
});
var counters = {},
    state = {},
    config = {},
    guard = {},
    smsConf = null,
    mailConf = {};
var button,
    counter = [0, 0],
    lastCounter = 0,
    zone,
    start,
    end,
    transporter,
    mailOptions = {};

var fsTimeout;
fs.watch(files['config'], function (event, filename) {
    if (!fsTimeout) {
        logger.debug('%s %s', filename, event);
        fsTimeout = setTimeout(function () {
            fsTimeout = null;
        }, 1000);
        if (filename == 'casa.conf') {
            readConfig(function (err, _conf) {
                if (err) {
                    logger.error('Error read casa.conf %s', err);
                } else {
                    config = _conf;
                    logger.debug('config loaded');
                }
            });
        }
    }
});
readFile(files['mail'], function (err, _mail) {
    if (err) {
        logger.error(err);
    } else {
        mailConf = _mail;
        //logger.debug(util.inspect(mailConf));
        transporter = nodemailer.createTransport({
            service: mailConf.provider,
            auth: {
                user: mailConf.user,
                pass: mailConf.pass
            }
        });

        mailOptions.from = 'Lobocano <lobocano@msn.com>';
        mailOptions.to = 'lobocanopau@gmail.com, lobocano@msn.com';
        mailOptions.subject = 'Alarm';
        mailOptions.text = 'Door opened';
        //mailOptions.html = "<h2>Door opened</h2>";
    };
});

exports.sendMail = function (text, subject, done) {
    var mOpts = mailOptions; /* {
                             from: mailOptions.from,
                             to: mailOptions.to,
                             subject: mailOptions.subject,
                             text: mailOptions.text
                             };*/

    mOpts.text = text;
    if (subject) mOpts.subject = subject;
    //logger.debug(util.inspect(mOpts));
    transporter.sendMail(mOpts, function (error, info) {
        //logger.debug(util.inspect(mailOptions));
        if (error) {
            logger.error(error);
            if (done) done(error);
        } else {
            logger.info('Message sent: ' + info.response);
            if (done) done();
        }
    });
};
// WebSocket server
/*var server = ws.createServer(function (conn) {
    logger.info("New connection");
    wsConn = conn;
    conn.on("text", function (str) {
        logger.debug("Received "+str);
        conn.sendText(str.toUpperCase()+"!!!");
    })
    conn.on("close", function (code, reason) {
        logger.info("Connection closed");
    })
}).listen(3003);
exports.wsConn = function(){
    return wsConn;
};*/

exports.logger = logger;
exports.files = files;
//exports.counters = function(){ return counters };
//exports.state = state;
exports.config = function () {
    return config;
};
exports.getZone = getZone;
function getZone() {
    return zone;
};
exports.updateZone = updateZone;
function updateZone() {
    zone = isDayZone() ? 0 : 1;return zone;
};
exports.isDayZone = isDayZone;

exports.readGuard = readGuard;
function readGuard(done) {
    readFile(files['guard'], done);
};
exports.writeGuard = function (data, done) {
    writeFile(files['guard'], data, done);
};

exports.readState = readState;
function readState(done) {
    readFile(files['state'], done);
};
exports.writeState = function (data, done) {
    writeFile(files['state'], data, done);
};

exports.readConfig = readConfig;
function readConfig(done) {
    readFile(files['config'], function (err, conf) {
        if (!err) {
            config = conf;
            var shift = config.electroCounterTimeShift;
            start = moment('07:00', 'HH:mm').add(shift, 'minutes');
            end = moment('23:00', 'HH:mm').add(shift, 'minutes');
            //start = moment(config.dayZone.start,"HH:mm");
            //end = moment(config.dayZone.end,"HH:mm");
            zone = isDayZone() ? 0 : 1;
            //logger.debug("shift: %d, start: %s end: %s zone: %d", shift, start, end, zone);
        }
        done(err, conf);
    });
};
exports.writeConfig = function (data, done) {
    writeFile(files['config'], data, done);
};

exports.readSmsConf = readSmsConf;
function readSmsConf(done) {
    readFile(files['sms'], function (err, data) {
        if (err) done(err);else {
            smsConf = data;
            done(err, data);
        };
    });
}
exports.sendSms = sendSms;
function sendSms(text, done) {
    if (smsConf == null) {
        readSmsConf(function (err, done) {
            if (err) {
                logger.error(err);
                done(err);
            } else send();
        });
    } else send();
    function send() {
        var sms = new SMSru(smsConf.recipients[0].api_id);
        var smsBody = { to: smsConf.recipients[0].to, text: text };
        logger.debug(util.inspect(smsBody));
        sms.sms_send(smsBody, function (e) {
            if (e) {

                if (e.description == 'Сообщение принято к отправке.') done();else {
                    logger.error(e.description);
                    done(e);
                }
            } else done();
        });
    }
}

exports.readCounters = readCounters;
function readCounters(done) {
    readFile(files['counters'], function (err, data) {
        if (err) done(err);else {
            //counters = data;
            //logger.debug(counters);
            done(err, data);
        };
    });
};
exports.writeCounters = writeCounters;
function writeCounters(data, done) {
    writeFile(files['counters'], data, done);
};

exports.composeState = function (done) {
    readConfig(function (err, _conf) {
        if (err) done(err);else {
            var guardOn = _conf.guardMode == 1;
            readGuard(function (err, _guard) {
                if (err) done(err);else {
                    guard = _guard;
                    readCounters(function (err, cntrs) {
                        if (err) done(err);else {
                            counters = cntrs;
                            readState(function (err, _state) {
                                var st = {};
                                if (err) {
                                    st = { // emulate PI
                                        time: new Date(),

                                        doors: [randOpen(), randOpen()],
                                        heaters: [randOn(), randOn(), randOn(), randOn()],
                                        boiler1: 'off',
                                        fire: fire(),
                                        water: water(),
                                        gas: gas(),
                                        temperatures: [10 + Math.random().toFixed(1) * 5, 20 + Math.random().toFixed(1) * 2, 20 + Math.random().toFixed(1) * 2, 15 + Math.random().toFixed(1) * 5]
                                    };
                                } else {
                                    state = _state;
                                    st = {
                                        time: new Date(),
                                        doors: [guard.doors[0].state == 0 ? 'c' : 'o', 'c'],
                                        fire: false,
                                        water: false,
                                        gas: false,
                                        temperatures: [state.t1, state.t5, state.t4, state.t3, state.t2, state.t6],
                                        heaters: [state.h1, 'off', 'off', state.h3],
                                        boiler1: state.wh1,
                                        P: state.P,
                                        ticks: counters.ticks,
                                        guardOn: guardOn
                                    };
                                }
                                var warn = st.doors[1] == 'o';
                                var alarm = st.doors[0] == 'o' && guardOn || st.temperatures[0] < 5 || st.fire || st.water || st.gas;
                                st.status = alarm ? 2 : warn ? 1 : 0;
                                done(null, st);
                            });
                        }
                    });
                }
            });
        }
    });
};

exports.composeCounters = function (day, night, coeff) {
    var counters = {
        ticks: [{ kWh: day, count: day * coeff }, { kWh: night, count: night * coeff }]
    };
    counters.counterSum = counters.ticks[0].count + counters.ticks[1].count;
    counters.lastMinuteCount = counters.counterSum - 30;
    counters.lastQuarterCount = counters.counterSum - 30 * 15;
    counters.lastHourCount = counters.counterSum - 30 * 60;
    counters.P = (30 / 6400 * 60).toFixed(2);
    counters.pwrAvgQuarter = ((counters.counterSum - counters.lastQuarterCount) / 6400 * 4).toFixed(2);
    counters.pwrAvgHour = (counters.counterSum - counters.lastHourCount) / 6400;
    writeCounters(counters, function (err) {
        if (err) logger.error('Error write counters:', err);
    });
};

function readFile(name, done) {
    jf.readFile(name, function (err, data) {
        if (err) {
            logger.error('Error: ' + err);
            done(err);
        } else {
            //data = JSON.parse(data);
            done(null, data);
        }
    });
}
function writeFile(name, data, done) {
    jf.writeFile(name, data, function (err) {
        if (err) {
            logger.error('Error: ' + err);
            done(err);
        } else {
            //data = JSON.parse(data);
            done();
        }
    });
}
function isDayZone() {
    var actTime = moment();
    return actTime > start && actTime < end;
}

function exit() {
    logger.info('exit');
    process.exit();
}
//emulation functions
function randOn() {
    //ToDo: change to real state of heaters
    return Math.random() > 0.5 ? 'on' : 'off';
}
function randOpen() {
    //ToDo: change to real sensors
    return Math.random() > 0.5 ? 'o' : 'c';
}

function fire() {
    //ToDo: change to real sensor
    return Math.random() > 0.57;
}
function water() {
    //ToDo: change to real sensor
    return Math.random() > 0.57;
}
function gas() {
    //ToDo: change to real sensor
    return Math.random() > 0.57;
}

//# sourceMappingURL=piutils.js.map

//# sourceMappingURL=piutils.js.map

//# sourceMappingURL=piutils.js.map