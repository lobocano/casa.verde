/**
 * Created by pablo on 28.05.15.
 */
'use strict';

exports.hw_emulation = true;
exports.socketRoomId = 'casa-verde158';
exports.socketUrl = 'http://lobocano.eu:3000/rooms';

var casaverde = process.env.HOME + '/casaverde/';

exports.files = {
    'state': casaverde + 'state.json',
    'config': casaverde + 'casa.conf',
    'log': casaverde + 'logs/measure.log',
    'counters': casaverde + 'counters.json',
    'guard': casaverde + 'guard.json',
    'sms': casaverde + 'sms.conf',
    'mail': casaverde + 'mail.conf',
    'thermostat': casaverde + 'thermostat.conf'
};

//# sourceMappingURL=constants.js.map

//# sourceMappingURL=constants.js.map

//# sourceMappingURL=constants.js.map

//# sourceMappingURL=constants.js.map