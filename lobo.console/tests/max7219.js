"use strict";

var MAX7219 = require("MAX7219");
var disp = new MAX7219("/dev/spidev0.0");
var d = false;
disp.setDecodeAll();
disp.setScanLimit(4);
disp.setDisplayIntensity(15);
disp.startup();
disp.setDigitSymbol(0, "2");
disp.setDigitSymbol(1, "3");
disp.setDigitSymbol(2, "5", true);
disp.setDigitSymbol(3, "6");

setInterval(function () {
    d = !d;
    var intens = d ? 8 : 1;
    disp.setDisplayIntensity(intens);
}, 1000);

//# sourceMappingURL=max7219.js.map