/**
 * Created by pablo on 07.03.16.
 */
var SerialPort = require("serialport").SerialPort
var serialPort = new SerialPort("/dev/ttyUSB0", {
    baudrate: 19200
}, false); // this is the openImmediately flag [default is true]
var readline = require('readline');
var term = require('terminal-kit').terminal;

var rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

var rcvBuffer = '';
serialPort.on('error', function (err) {
    console.log(err);
});

serialPort.open(function (err) {
    if (err) {
        console.log(err);
        return;
    }
    console.log('open');
    term.eraseDisplay();
    serialPort.on('data', function (data) {
        if (data != '\n') rcvBuffer += data;
        else {
            term.moveTo(1, 1);
            term.bold().green('data received: %s', rcvBuffer);
            term.moveTo(1, 2).eraseLine();
            //term.moveTo(1,3);
            //console.log();
            rcvBuffer = '';
        }

    });
    /*
     setInterval(function () {
     serialPort.write((new Date()) + "\n", function (err, results) {
     });

     }, 1000);
     */
});
rl.on('line', lineHandler);
rl.on('SIGINT', sigint);

function lineHandler(line) {
    term.moveTo(1, 2).bold(line);
    serialPort.write(line + "\n", function (err, results) {
    });
    term.moveTo(1, 3).eraseLine();
}
function sigint() {
    rl.close();
    process.exit(103);
}
/*
 var SerialPort = require("serialport");//.SerialPort;
 SerialPort.list(function (err, ports) {
 ports.forEach(function(port) {
 console.log(port.comName);
 console.log(port.pnpId);
 console.log(port.manufacturer);
 });
 });
 */
/*

 */
