/**
 * Created by pablo on 08.10.14.
 */
// electro counter - GPIO24

const constants = require('./constants');
const moment = require( 'moment');
const fs = require( 'fs');
const jf = require( 'jsonfile');
const piutils = require( './piutils');

var Gpio = constants.hw_emulation ? undefined : require('onoff').Gpio;// require('onoff').Gpio;

var db = require('arango').Connection('http://localhost:8529');


var thermoReg = require('./themp.js');
var guard = require('./guard.js');
var sockets = require('./sockets');

var logger = piutils.logger;
//var configFile = '/home/pi/casaverde/casa.conf';
const stateFile = constants.files['state'],
    countersFile = constants.files['counters'];
var casaConfig = {},
    counters = {};

// run before: gpio-admin export 24
var button;// = new Gpio(24, 'in', 'rising');

var counter = [0,0],
    counterSum = 0,
    lastCounter = 0;

var zone, config, state;
var ms = moment();
/* guard */
var doorSensor;
var lastDoorOpen;
var countPeriod = 60;

//measureInit();// start loop
exports.measureInit = measureInit;


function exit() {
    logger.info('exit');
    process.exit();
}
function isDayZone(){
    actTime = moment();

    //logger.debug("start: %s end: %s act: %s", start, end, actTime.local().format("HH:mm"));
    return actTime > start && actTime < end;
}

//process.on('SIGINT', exit);
function measureInit(){
    piutils.readConfig(function(err, conf){
        if (err) {
            logger.error('Error read casa.conf %s',err);
            exit();
        }
        else {
            config = conf;
            piutils.readCounters(function(err, _cnt){
                if( err ){
                    logger.error('Error read counters: %s', err);
                    exit();
                }
                else{
                    counters = _cnt;
                    if(counters.counterSum) counterSum = counters.counterSum;
                    else counters.counterSum = counterSum;
                    if(!counters.lastMinuteCount) {
                        counters.lastMinuteCount = counterSum;
                    }

                    process.on('SIGINT', exit);
                    zone = piutils.getZone();
                    //button = new Gpio(24, 'in', 'rising');
                    ms = moment();
                    if(!constants.hw_emulation){
                        button = new Gpio(24, 'in', 'falling',{ debounceTimeout: 25 });

                        button.watch(function(err, value) {
                            if (err) {
                                logger.error('Error watch: %s', err);
                                exit();
                            }
                            counter[zone]++;
                            counterSum++;
                        });
                        guard.initGuard(config);

                    }
                    else {
                        setInterval(function(){
                            counter[zone]++;
                            counterSum++;
                        },1000);
                    }
                    countElectro();
                    setInterval(countElectro, countPeriod * 1000);

                    logger.info( '*** start loop ***');
                    saveInitMsg();
                }
                function saveInitMsg(){
                    db.document.create('InitMsg',{fecha: new Date(), source: 'measureInit', msg:'Loop started'}, {createCollection:true})
                        .then(function(res){
                            logger.debug("InitMsg saved");

                        },function(err){
                            logger.error("InitMsg err: %s", err);

                        })
                }
            });
        }
    });
}
function countElectro(){
    piutils.readConfig(function(err, conf){
        if (err) {
            logger.error('Error read casa.conf %s',err);
            exit();
        }
        else {
            config = conf;
            piutils.readState(function(err, _state){
                if( err ){
                    logger.error('Error read state: %s', err);
                    exit();
                }
                else{
                    state = _state;
                    piutils.readCounters(function(err, _cnt){
                        if( err ){
                            logger.error('Error read counters: %s', err);
                            exit();
                        }
                        else{
                            counters = _cnt;
                            zone = piutils.updateZone();

                            //var ticksPerPeriod = counter[zone] - lastCounter;
                            var ticksPerPeriod = counterSum - counters.lastMinuteCount;
                            counters.lastMinuteCount = counterSum;
                            counters.counterSum = counterSum;
                            //var totalCount = counters.ticks[0].count + counters.ticks[1].count;
                            var mins = moment().minute();
                            if(mins % 15 == 0){
                                if(!counters.lastQuarterCount) {
                                    counters.lastQuarterCount = counterSum;
                                    counters.lastHourCount = counterSum;
                                }
                                else {
                                    counters.pwrAvgQuarter = (counterSum - counters.lastQuarterCount) / 6400 * 4;
                                    counters.lastQuarterCount = counterSum;
                                    //saveAvgHourPower();// temp
                                }
                            }
                            if(mins == 0){
                                if(!counters.lastHourCount) {
                                    counters.lastHourCount = counterSum;
                                }
                                else{
                                    counters.pwrAvgHour = (counterSum - counters.lastHourCount) / 6400;
                                    counters.lastHourCount = counterSum;
                                    saveAvgHourPower();
                                }
                            }

                            //var zone = isDay ? 0 : 1;
                            if (counters.ticks) {
                                counters.ticks[zone].count += ticksPerPeriod;
                            }
                            else {
                                counters.ticks = [
                                    {count: counter[0], kWh: 0},
                                    {count: counter[1], kWh: 0}
                                ];
                                //logger.debug('state.ticks created:%j', counters.ticks);
                                lastCounter = 0;
                            }

                            lastCounter = counter[zone];
                            var kWh = (counters.ticks[zone].count / 6400).toFixed(2);
                            //logger.debug('kWh:%d ', kWh);
                            var P = (ticksPerPeriod / 6400 * 60).toFixed(2);

                            counters.P = P;

                            counters.ticks[zone].kWh = kWh;
                            //logger.info("start:%s, end:%s, actual:%s", start, end, moment().format());
                            state.P = counters.P;
                            state.ticks = counters.ticks;
                            /*logger.info('ticks: %d, P(kW): %d, day: %d %d kW-h, night: %d %d kW-h',
                                ticksPerPeriod,P,
                                counters.ticks[0].count, counters.ticks[0].kWh,
                                counters.ticks[1].count,counters.ticks[1].kWh);*/
                            logger.info('ticks: %d, P(kW): %d', ticksPerPeriod,P);

                            jf.writeFile(stateFile, state, function (err) {
                                if (err) logger.error('Error write state file: %j', err);
                                jf.writeFile(countersFile, counters, function (err) {
                                    if (err) logger.error('Error write counters file: %j', err);
                                    thermoReg.setConfStateCounts(config, state, counters);
                                    //thermoReg.thermoReg();
                                });
                            });
                            if(mins == 1 && moment().hour() == 0) {
                                var cCounts = {fecha:new Date(), day:parseFloat(counters.ticks[0].kWh), nigth:parseFloat(counters.ticks[1].kWh)};
                                logger.info('save current count:',cCounts);
                                db.document.create('Counter',cCounts, {createCollection:true})
                                    .then(function(res){
                                        logger.debug("done add Counter: ",res);
                                        //logger.info("done add themp: %j",res);
                                        //done();
                                    },function(err){
                                        logger.error("err: ", err);
                                        //logger.error("err: %j", err);
                                        //done(err);
                                    })

                            }
                        }

                    });
                }
            });

        }
    });

    /*zone = piutils.updateZone();
    fs.readFile(stateFile, 'utf8', function (err, data) {
        if(err){
            logger.error('Error read %s %s',stateFile,err)

        }
        else {


            state = JSON.parse(data);

         }
    });*/

}
function saveAvgHourPower(){
    var doc = {
        fecha:new Date(),
        power: counters.pwrAvgHour
    };
    db.document.create('AvgPower',doc, {createCollection:true})
        .then(function(res){
            logger.debug("done add AvgPower: %j",res);
            //logger.info("done add themp: %j",res);
            //done();
        },function(err){
            logger.error("err: %j", err);
            //logger.error("err: %j", err);
            //done(err);
        })
}



