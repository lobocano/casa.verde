/**
 * Created by pablo on 06.10.14.
 */
var winston = require('winston');
module.exports = new (winston.Logger)({
    transports: [
        new (winston.transports.Console)({ level: 'debug'}),
        new (winston.transports.File)({ filename: '/home/pi/casaverde/picon.log', level: 'info' })
    ]
});
//logger.log('info', 'Hello distributed log files!');
//logger.error('Hello again distributed logs');

//exports.logger = logger;