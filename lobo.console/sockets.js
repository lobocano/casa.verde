/**
 * Created by pablo on 02.02.15.
 */
var constants = require('./constants'),
    piutils = require('./piutils');

exports.io;
exports.connection;
//var aa;
exports.aa;
exports.create = function(server){
    exports.io = require('socket.io')(server);
    exports.io.on('connection', function(socket){
        console.log('a user connected');

        exports.connection = socket;
        //aa = {data:'sss'};
        //console.log(connection);
        socket.on('disconnect', function(){
            console.log('user disconnected');
            exports.connection = null;
        });
    });
}


module.exports.Client = function(){
    //var intrvId;
    var self = this;
    self.connected = false;
    self.isRoomEmpty = true;
    self.client = require('socket.io/node_modules/socket.io-client')(constants.socketUrl);
    self.client.on('connect', function(){
        console.log('connected');
        self.connected = true;
        self.client.emit('clientId',constants.socketRoomId);

        /*intrvId = setInterval(function(){
            //console.log('set interval', new Date());
            self.client.emit('event:state',{fecha:new Date()});
            self.client.emit('event:guard','guard emitted');
        }, 10000);*/
    });
    self.client.on('disconnect', function(){
        console.log('disconnected');
        self.connected = false;
        //clearInterval(intrvId);
    });
    self.client.on('event:roomCount', function(count){
        console.log('event:roomCount ', count);
        if(count > 1) self.isRoomEmpty = false;
        else self.isRoomEmpty = true;
        //clearInterval(intrvId);
    });
    self.client.on('event:getStatus', function(){
        console.log('event:getStatus');
        piutils.composeState(function(err, state){
            //console.log('state:', state);
            var status = {
                fecha:new Date(),
                ticks: state.ticks,
                temperatures: state.temperatures,
                heaters: state.heaters,
                boiler1: state.boiler1,
                P: state.P,
                doors: state.doors
            };
            //status.temperatures[0] = 25.6;
            //console.log('status', status);
            self.client.emit('msg:state',status);
            console.log('emit msg:state');
        })
    });
    //console.log(bb);
    //return this;
}
var instance = null;

module.exports.getInstance = function(){
    if(instance === null){
        instance = new module.exports.Client();
    }
    return instance;
}

/*module.exports = function() {
    var io = {};
    var connection = null;
    var aa;
    return {
        b:15,
        connection: {},
        create: function(server) {
            console.log(server);
        },
        a:function(d){
            if(d) aa = d;
            else return aa;
        }
    };
}*/