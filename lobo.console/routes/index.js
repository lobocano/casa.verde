'use strict';

var express = require('express');
var router = express.Router();
var path = require('path');

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', { title: 'Express-Angular2' });
});
router.get('/1', function (req, res, next) {
  res.render('index3', { title: 'Express' });
});
router.get('/partials/*', function (req, res, next) {
  var requestedView = path.join('./', req.url);
  res.render(requestedView);
});
module.exports = router;

//# sourceMappingURL=index.js.map

//# sourceMappingURL=index.js.map

//# sourceMappingURL=index.js.map

//# sourceMappingURL=index.js.map