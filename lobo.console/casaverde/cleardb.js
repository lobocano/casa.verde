"use strict";

dt = new Date();
dt.setTime(dt.getTime() - 86400000 * 3 + 3600000 * 12);
print(dt);
db._query("FOR t in ThempHist FILTER DATE_TIMESTAMP(t.fecha) < DATE_TIMESTAMP(@dt) REMOVE t in ThempHist", { "dt": dt });
db._query("FOR t in AvgPower FILTER DATE_TIMESTAMP(t.fecha) < DATE_TIMESTAMP(@dt) REMOVE t in AvgPower", { "dt": dt });
db._query("FOR t in OnOff FILTER DATE_TIMESTAMP(t.fecha) < DATE_TIMESTAMP(@dt) REMOVE t in OnOff", { "dt": dt });
db._query("FOR t in InitMsg FILTER DATE_TIMESTAMP(t.fecha) < DATE_TIMESTAMP(@dt) REMOVE t in InitMsg", { "dt": dt });

//# sourceMappingURL=cleardb.js.map

//# sourceMappingURL=cleardb.js.map

//# sourceMappingURL=cleardb.js.map

//# sourceMappingURL=cleardb.js.map