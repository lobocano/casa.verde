var constants = require('./constants');
var Gpio = {}, sensor = {};
if(!constants.hw_emulation){
    Gpio = require('onoff').Gpio;
    sensor = require('ds18x20');

}


const maxCycles = 100;
var cyclesCounter = 0;
var intervalHandler = {};
var devices = {};
intervalHandler = setInterval(() => {
    if(cyclesCounter >= maxCycles){
        cyclesCounter = 0;
     }
    if(devices){
        for (var property in devices){
            if(devices[property]){
                if(cyclesCounter == devices[property].Offset){
                    devices[property]._cyclesCounter = 0;
                }
                if(devices[property]._cyclesCounter == 0) {
                    devices[property].handlePower(true);
                    if(devices[property].getThemperature) devices[property].getThemperature();
                } // start pulse
                // }
                else {
                    var pulseWidth = Math.ceil(devices[property].Power * maxCycles / 100 ) ;
                    if(devices[property]._cyclesCounter >= pulseWidth){
                        devices[property].handlePower(false); // end pulse
                    }
                }
                if(devices[property]) devices[property]._cyclesCounter++;

            }

        }

    }
    cyclesCounter ++;
}, 20);


class HeatDevice{

    constructor(name, offset, gpioNum){
        this._isOn = false;
        this._power = 0; // 0...100 percent

        this._name = name;
        this._offset = offset;
        this._gpioNum = gpioNum;
        this._gpioOn = false;
        this._cyclesCounter = 0;
    }
    get isOn(){
        return this._isOn;
    }
    set isOn(value){
        this._isOn = value;
        if(value){
            devices[this._name] = this;
            if(!constants.hw_emulation){
                this._gpio = new Gpio(this._gpioNum, 'out');
            }

        }
        else {
            this._gpioOn = false;
            if(!constants.hw_emulation) this._gpio.writeSync(0);
            delete(devices[this._name]);
        }
    }
    get Power(){
        return this._power;
    }
    set Power(value){
        this._power = value;
    }
    setPower(p){
        this._power = p;
    }
    get Offset(){ return this._offset};

    handlePower(val) {
        if(val){

            if(!this._gpioOn && this._power > 0){
                //console.log(this._name,'on',this._power);
                this._gpioOn = true;
                if(!constants.hw_emulation) this._gpio.writeSync(1);
            }
        }
        else{
            if(this._power != 100){
                if(this._gpioOn){
                    //console.log(this._name,'off',this._power);
                    this._gpioOn = false;
                    if(!constants.hw_emulation) this._gpio.writeSync(0);
                }
            }
        }
        //console.log(val);
    }

}
class Thermostat extends HeatDevice {
    constructor(name, offset, gpioNum, dsId, gain, terminal, loopback){
        super(name, offset, gpioNum);
        this._sensorId = dsId;
        this._loopGain = gain;
        this._tMax = 20;
        this._tCurrent = 10;
        this._loopback = loopback;
        if(terminal) this.terminal = terminal;
    }

    getThemperature(){
        if(!constants.hw_emulation) {
            sensor.getAll((err, tList) => {
                if (err) console.log(err);
                else {

                    if (tList[this._sensorId]) {

                        this._tCurrent = parseFloat(tList[this._sensorId]);
                        var delta = this._tMax - this._tCurrent;
                        if (this._loopback) {
                            if (delta > 0) {
                                var percentGrano = 100 / maxCycles;
                                var pwr = Math.ceil(delta * this._loopGain);
                                pwr += (pwr % percentGrano);
                                if (pwr > 100) pwr = 100;
                                this._power = pwr;
                            }
                            else {
                                this._power = 0;
                            }
                        }
                        if (this.terminal) this.terminal(this._tMax, this._tCurrent, delta, this._power);
                        else  console.log("%d %s %d", this._tCurrent, delta.toFixed(2), this._power);
                    }
                    else {
                        console.log(this._sensorId, tList);
                    }
                }

            });
        }
        else{
            this._tCurrent = 25; //this._power = 0;
            var delta = this._tMax - this._tCurrent;
            if (this.terminal) this.terminal(this._tMax, this._tCurrent, delta, this._power);
            else  console.log("%d %s %d", this._tCurrent, delta.toFixed(2), this._power);

        }
    }
    set Tmax(val){
        this._tMax = val;
    }
    get Tmax(){
        return this._tMax;
    }
}
/*exports.addDevice = function(name, offset){
    devices[name] = new HeatDevice(name, offset);
    return devices[name];
}
exports.removeDevice = function(name){
    delete(devices[name]);
}*/
exports.HeatDevice = HeatDevice;
exports.Thermostat = Thermostat;
exports.devices = devices;