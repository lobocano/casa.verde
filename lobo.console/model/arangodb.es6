/**
 * Created by pablo on 05.06.15.
 */

var Database = require('arangojs');
export var db = new Database({databaseName:'casaverde',url:'http://pi:@localhost:8529'});



//var rep = db.collection('test1');
//console.log(rep);
export class Repository{
    constructor(collection){
        this._collection = db.collection(collection, true);
        this._collection.then(function(coll){
            console.log(coll.name);
        }, function(err){
            console.error(err);

        });
     }
    Save(document, key){
        if(key) document._key = key;
        return this._collection.then(function(coll){
            return  coll.save(document);
        }, function(err){
            return  Promise.reject(err);

        });
    }
    Read(key){
        return this._collection.then(function(coll){
            return  coll.document(key);
        }, function(err){
            return  Promise.reject(err);

        });
    }
    Update(key, data){
        return this._collection.then(function(coll){
            return  coll.update(key, data);
        }, function(err){
            return  Promise.reject(err);

        });

    }
    Delete(key){
        return this._collection.then(function(coll){
            return  coll.remove(key);
        }, function(err){
            return  Promise.reject(err);

        });
    }
    Select(query){
        return db.query(query)
            .then(function(cursor) {
                return cursor.all()
            },
            function(err){ return Promise.reject(err); }
        );

    }
}

//export Repository;

/*createDoc({name:'test3'});
setTimeout(function(){
    createDoc({name:'test4'});
}, 2000);
function createDb(){
    db.createDatabase('casaverde', [{username: 'pi'}], function (err, database) {
        if (err) return console.error(err);
        console.log('created');
        // database is a Database instance
    });
}*/
function createColl(){
    var promise = db.createCollection('test1');

    promise.then(
        function(coll){
            console.log('created: ', coll.name);
            return 200;
        },
        function(err){
            console.error(err.message);
            return 404;
        }
    )
        .then(function(data){
            console.log(data);
        });
}
function createDoc(doc){
    rep
    .then(function(coll){
            console.log('get coll: ', coll.name);
            return coll.save(doc);
        },
    function(err){
        console.error(err.message);
    })
    .then(function(res){
            console.log(res);
        },
    function(err){
        console.error(err.message);
    })
}

class Themp {
    constructor(id, fecha, themp, room2, outdoor, cellar, kitchen, p, z0, z1){
        this.sensorId = id;
        this.fecha = fecha;
        this.themp = themp;
        this.room2 = room2;
        this.outdoor = outdoor;
        this.cellar = cellar;
        this.kitchen = kitchen;
        this.P = p;
        this.ticks0 = z0;
        this.ticks1 = z1;
    }

}
