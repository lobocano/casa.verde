'use strict';

var constants = require('./constants');
//var Gpio = require('onoff').Gpio;
var Gpio = constants.hw_emulation ? {} : require('onoff').Gpio;
var piutils = require('./piutils');
var logger = piutils.logger;
var arango = require('arango'),
    db = arango.Connection('http://localhost:8529');
var sensor = constants.hw_emulation ? {} : require('ds18x20'),
    id = '28-0000047c1fb7';
var moment = require('moment');
var fs = require('fs'),
    jf = require('jsonfile');
var sockets = require('./sockets');
//var socketClient = new sockets.Client();
var socketClient = sockets.getInstance();

var casaConfig = {};
var stateFile = constants.files['state'];
var state = {};
var heatModes = ['Off', 'No Frozen', 'Normal'];
var led, tubeHeater, boilerHeater, thermostatHeater;
if (!constants.hw_emulation) {
    led = new Gpio(11, 'out');
    tubeHeater = new Gpio(9, 'out');
    boilerHeater = new Gpio(17, 'out');
    thermostatHeater = new Gpio(27, 'out');
}

var counts, zone;
var thermoRegStarted = false;

//console.log(new Date());
//console.error("test error");
var histMin = 22,
    histMax = 23,
    aver = 22.5;
//startMeasure();

// power control
var pwmMode = false;
var cyclesCount = 0,
    pwInterval,
    maxCycles = 50,
    waterHeaterPower = 80,
    mainHeaterPower = 60;
var heaters = [{ name: 'boiler', power: 0, gpio: boilerHeater, val: 0, loopCoeff: 1 }, { name: 'main', power: 0, gpio: led, val: 0, loopCoeff: 200 }, { name: 'thermostat', power: 0, gpio: thermostatHeater, val: 0, loopCoeff: 200 }];
if (pwmMode) {
    pwInterval = setInterval(powerCallback, 20);
}

//logger.debug('pwInterval');
function powerCallback() {
    heaters.forEach(function (val) {

        var ticks = Math.ceil(val.power * maxCycles / 100);
        //console.log(ticks);
        if (cyclesCount >= ticks) {
            if (val.power != 100 && val.val == 1) {
                val.val = 0;
                val.gpio.writeSync(0);
                //logger.debug(val.name, val.val);
            }
        }
        //console.log(val);
    });
    if (cyclesCount >= maxCycles) {
        cyclesCount = 0;
        heaters.forEach(function (val) {
            if (val.power > 0) {
                val.val = 1;
                val.gpio.writeSync(1);
                //logger.debug(val.name, val.val);
            }
            //console.log(val);
        });
    }
    cyclesCount++;
}
// power control end

function Themp(id, fecha, themp, room2, outdoor, cellar, kitchen, p, z0, z1) {
    this.sensorId = id;
    this.fecha = fecha;
    this.themp = themp;
    this.room2 = room2;
    this.outdoor = outdoor;
    this.cellar = cellar;
    this.kitchen = kitchen;
    this.P = p;
    this.ticks0 = z0;
    this.ticks1 = z1;
}

function dbAddThemp(id, thempMsr, done) {
    //var dt = new Date();
    var mins = moment().minute();
    var secs = moment().second();
    //logger.debug('minute: %d',mins);
    var isWriteTime = secs >= 0 && secs < 15 && mins % 15 == 0;
    //logger.debug("isWriteTime:%d",isWriteTime);
    //var themps = [state.t1, state.t2];

    if (isWriteTime) {
        logger.debug('store themp');
        var p = counts.pwrAvgQuarter ? counts.pwrAvgQuarter : counts.P;
        var cellar = state.t3 ? state.t3 : 0;
        var kitchen = state.t4 ? state.t4 : 0;
        var room2 = state.t5 ? state.t5 : 0;
        var thmp = new Themp(id, new Date(), thempMsr, room2, state.t2, cellar, kitchen, p, counts.ticks[0].count, counts.ticks[1].count);
        //if(state.P) thmp.P = state.P;
        /*if(state.ticks) {
            thmp.ticks0 = state.ticks[0].count;
            thmp.ticks1 = state.ticks[1].count;
        }*/
        //console.log("themps:%j",themps);
        //console.log(thmp);
        db.document.create('ThempHist', thmp, { createCollection: true }).then(function (res) {
            //logger.debug("done add themp: %j",res);
            //logger.info("done add themp: %j",res);
            done();
        }, function (err) {
            logger.error('err: %j', err);
            //logger.error("err: %j", err);
            done(err);
        });
    } else {
        done();
    }
}

function getThemp() {
    var outThemp, cellarThemp, kitchenThemp, room2Themp, thermostatThemp;
    var room1OnOff, room2OnOff, cellarOnOff, thermostatOnOff;
    var ods = casaConfig.outDoorSensorId;
    var cellar = casaConfig.cellar.sensorId;
    var kitchen = casaConfig.kitchen.sensorId;
    var thermostat = casaConfig.thermostat.sensorId;
    var room2;
    //var thempsAll = sensor.getAll();
    //logger.debug("thempsAll: %s",thempsAll);

    if (constants.hw_emulation) {
        state.t2 = '10.2';
        state.t3 = '8.0';
        state.t4 = '20.3';
        state.t1 = '22.1';
        state.t5 = '18.2';
        state.t6 = '29.0';
        if (socketClient.connected && !socketClient.isRoomEmpty) {
            socketClient.client.emit('msg:state', createStatus());
            console.log('emit: ');
        }
        return;
    }
    var thempsAll;
    sensor.getAll(getAllCallback);
    function getAllCallback(err, tList) {
        if (err) logger.error('getAllCallback: ', err);
        //else logger.debug(tList);
        try {
            var sId;
            var thempMsr;

            (function () {
                var thermostatHandler = function () {
                    if (casaConfig.thermostat.mode == 0) {
                        // off
                        state.h6 = 'off';
                        var oldThermostat = parseInt(thermostatHeater.readSync());
                        //heaters[2].power = 0;
                        thermostatHeater.writeSync(0);
                        thermostatOnOff = 'off';
                    } else if (casaConfig.thermostat.mode == 1) {
                        //on
                        thermostatOnOff = '';
                        var delta = casaConfig.thermostat.tMax - state.t6;
                        if (delta > 0.5) {
                            thermostatHeater.writeSync(1);
                            thermostatOnOff = 'on';
                            state.h6 = 'on'
                        } else {
                            thermostatHeater.writeSync(0);
                            thermostatOnOff = 'off';
                            state.h6 = 'off';
                        }
                    }
                };
                var thermostatPWM = function () {
                    if (casaConfig.thermostat.mode == 0) {
                        // off
                        state.h6 = 'off';
                        heaters[2].power = 0;
                        thermostatOnOff = 'off';
                    } else if (casaConfig.thermostat.mode == 1) {
                        //on
                        thermostatOnOff = '';
                        var delta = casaConfig.thermostat.tMax - state.t6;
                        if (delta > 0) {
                            var percentGrano = 100 / maxCycles;
                            var pwr = Math.ceil(delta * heaters[2].loopCoeff);
                            pwr += pwr % percentGrano;
                            if (pwr > 100) pwr = 100;
                            heaters[2].power = pwr;
                            thermostatOnOff = 'on';
                            state.h6 = 'on';
                        } else {
                            heaters[2].power = 0;
                            thermostatOnOff = 'off';
                            state.h6 = 'off';
                        }
                    }
                };

                var mainHeatPWM = function (done) {
                    if (casaConfig.heatMode == 1) {
                        // no frozen
                        var nofraver = zone == 0 ? casaConfig.noFrozenThemp : casaConfig.noFrozenThemp + 3;
                        histMin = nofraver - casaConfig.hysteresis / 2;
                        histMax = nofraver + casaConfig.hysteresis / 2;
                        aver = nofraver;
                    }
                    if (casaConfig.heatMode == 0) {
                        // off
                        state.h1 = 'off';
                        //led.writeSync(0);
                        heaters[1].power = 0;
                        room1OnOff = 'off';
                    } else {
                        //normal
                        //var oldH1;
                        room1OnOff = '';
                        var delta = aver - state.t1;
                        if (delta > 0) {
                            var percentGrano = 100 / maxCycles;
                            var pwr = Math.ceil(delta * heaters[1].loopCoeff);
                            pwr += pwr % percentGrano;
                            if (pwr > 100) pwr = 100;
                            //logger.debug('power correct:%d %d', delta, pwr);
                            heaters[1].power = pwr;
                            room1OnOff = 'on';
                            state.h1 = 'on';
                        } else {
                            heaters[1].power = 0;
                            room1OnOff = 'off';
                            state.h1 = 'off';
                        }
                        /*if(state.t1 < histMin) {
                             //oldH1 = parseInt(led.readSync());
                             //if(oldH1 == 0){
                                logger.debug("low: %d min: %d",state.t1, histMin);
                                logger.info("on");
                                state.h1 = "on";
                                heaters[1].power = mainHeaterPower;
                                //led.writeSync(1);
                                room1OnOff = ''on;
                            //}
                         }
                        else if (state.t1 > histMax) {
                            //oldH1 =  parseInt(led.readSync());
                             //if(oldH1 == 1) {
                                logger.debug("high: %d max: %d",state.t1, histMax);
                                logger.info("off");
                                state.h1 = "off";
                                //led.writeSync(0);
                                heaters[1].power = 0;
                                room1OnOff = 'off';
                            //}
                        }*/
                    }
                };

                var mainHeat = function (done) {
                    if (casaConfig.heatMode == 1) {
                        var nofraver = zone == 0 ? casaConfig.noFrozenThemp : casaConfig.noFrozenThemp + 3;
                        histMin = nofraver - casaConfig.hysteresis / 2;
                        histMax = nofraver + casaConfig.hysteresis / 2;
                    }
                    if (casaConfig.heatMode == 0) {
                        //logger.debug("off: %d",state.t1);
                        state.h1 = 'off';
                        //led = new Gpio(11, 'out');
                        led.writeSync(0);
                        room1OnOff = 'off';
                    } else {
                        var oldH1;
                        room1OnOff = '';
                        if (state.t1 < histMin) {
                            oldH1 = parseInt(led.readSync());

                            if (oldH1 == 0) {
                                logger.debug('low: %d min: %d, current:%d', state.t1, histMin, oldH1);
                                logger.info('on');
                                state.h1 = 'on';
                                led.writeSync(1);
                                room1OnOff = 'on';
                            }
                        } else if (state.t1 > histMax) {
                            oldH1 = parseInt(led.readSync());

                            if (oldH1 == 1) {
                                logger.debug('high: %d max: %d, current:%d', state.t1, histMax, oldH1);
                                logger.info('off');
                                state.h1 = 'off';
                                led.writeSync(0);
                                room1OnOff = 'off';
                            }
                        }
                    }
                };

                // tube heat

                var tubeHeat = function (done) {
                    var tubeLow = casaConfig.cellar.tubeMin;
                    cellarOnOff = '';
                    var oldTube = parseInt(tubeHeater.readSync());
                    if (cellarThemp && cellarThemp < tubeLow) {
                        if (oldTube == 0) {
                            state.h3 = 'on';
                            logger.debug('tube on:%d %d %d', cellarThemp, tubeLow, oldTube);
                            tubeHeater.writeSync(1);
                            cellarOnOff = 'on';
                        }
                    } else if (cellarThemp && cellarThemp > tubeLow) {
                        if (oldTube == 1) {
                            state.h3 = 'off';
                            logger.debug('tube off:%d %d %d', cellarThemp, tubeLow, oldTube);
                            tubeHeater.writeSync(0);
                            cellarOnOff = 'off';
                        }
                    }
                };

                var waterHeatPWM = function (done) {
                    if (casaConfig.boilerMode == 0) {
                        // off mode
                        heaters[0].power = 0;
                        state.wh1 = 'off';
                    } else if (casaConfig.boilerMode == 2) {
                        // on mode
                        heaters[0].power = waterHeaterPower;
                        state.wh1 = 'on';
                    } else {
                        //econom mode
                        var hora = moment().hour();
                        if (hora >= 0 && hora <= 6) {
                            heaters[0].power = 80;
                            state.wh1 = 'on';
                        } else {
                            heaters[0].power = 0;
                            state.wh1 = 'off';
                        }
                    }
                };

                var waterHeat = function (done) {

                    var oldBoilerState = parseInt(boilerHeater.readSync());
                    //logger.debug("waterHeat: %d %d",casaConfig.boilerMode, oldBoilerState);
                    if (casaConfig.boilerMode == 0) {
                        // off mode
                        boilerHeater.writeSync(0);
                        state.wh1 = 'off';
                    } else if (casaConfig.boilerMode == 2) {
                        // on mode
                        if (oldBoilerState == 0) {
                            boilerHeater.writeSync(1);
                            state.wh1 = 'on';
                        }
                    } else {
                        //econom mode
                        var hora = moment().hour();
                        if (hora == 6) {
                            if (oldBoilerState == 0) {
                                boilerHeater.writeSync(1);
                                state.wh1 = 'on';
                            }
                        } else {
                            if (oldBoilerState == 1) {
                                boilerHeater.writeSync(0);
                                state.wh1 = 'off';
                            }
                        }
                    }
                };

                var thempFinish = function () {

                    jf.writeFile(stateFile, state, function (err) {
                        if (err) console.log(err);
                        dbAddThemp(id, state.t1, function (err) {
                            if (err) logger.error('err: %j', err);
                            //else logger.debug("ThermoReg done");
                        });
                    });
                    /*if(room1OnOff != ''){
                        saveOnOff('room1OnOff', room1OnOff)
                    }*/
                    if (cellarOnOff != '') {
                        saveOnOff('cellarOnOff', cellarOnOff);
                    }
                };

                var saveOnOff = function (source, val) {
                    db.document.create('OnOff', { fecha: new Date(), source: source, msg: val }, { createCollection: true }).then(function (res) {
                        logger.debug('OnOff saved');
                    }, function (err) {
                        logger.error('OnOff err: %s', err);
                    });
                };

                thempsAll = tList;
                //logger.debug('thempsAll', thempsAll);

                if (ods != 'not connected') {
                    //28-0000047bb6f3
                    //var tmp = thempsAll[ods];
                    state.t2 = thempsAll[ods];
                    outThemp = parseFloat(state.t2);
                    //state.t2 = sensor.get(ods);
                    //outThemp = parseFloat(state.t2);

                    //logger.debug("Outdoor themp: %d",state.t2);
                    //logger.debug("Outdoor themp: %d",tmp);
                }
                if (cellar != 'not connected') {
                    //28-0000066e5867
                    //state.t3 = sensor.get(cellar);
                    //cellarThemp = parseFloat(state.t3);
                    state.t3 = thempsAll[cellar];
                    cellarThemp = parseFloat(state.t3);
                    //logger.debug("Cellar themp: %d",state.t3);
                }
                if (kitchen != 'not connected') {
                    //28-0000066ff4ca
                    //state.t4 = sensor.get(kitchen);
                    state.t4 = thempsAll[kitchen];
                    kitchenThemp = parseFloat(state.t4);
                    //logger.debug("Kitchen themp: %d",state.t4);
                }
                if (thermostat != 'not connected') {
                    //28-0000066ff4ca
                    //state.t4 = sensor.get(kitchen);
                    state.t6 = thempsAll[thermostat];
                    thermostatThemp = parseFloat(state.t6);
                    //logger.debug("thermostat themp: %d",state.t6);
                }
                //
                sId = casaConfig.rooms[0].sensorId;

                //var thempMsr = sensor.get(sId);
                thempMsr = thempsAll[sId];

                state.t1 = (thempMsr + casaConfig.overheat).toFixed(1);
                if (casaConfig.rooms[1]) {
                    room2 = casaConfig.rooms[1].sensorId;
                    if (room2 != 'not connected') {
                        //28-0000066fcef5
                        //state.t5 = sensor.get(room2);
                        state.t5 = thempsAll[room2];
                        room2Themp = parseFloat(state.t5);
                        //logger.debug("Room2 themp: %d",state.t5);
                    }
                }
                if (socketClient.connected && !socketClient.isRoomEmpty) {}
                //logger.debug("heatMode: %s",heatModes[casaConfig.heatMode]);
                //logger.debug("noFrozenThemp: %s",casaConfig.noFrozenThemp);

                if (pwmMode) {
                    mainHeatPWM();
                    thermostatPWM();
                } else {
                    mainHeat();
                    thermostatHandler()
                }

                tubeHeat();
                if (pwmMode) {
                    waterHeatPWM();
                } else {
                    waterHeat();
                }
                //logger.debug("Themp: %d, %d, %d, %d, %d", state.t1, state.t5, state.t4, state.t3, state.t2);
                if (pwmMode) {
                    logger.debug('T C : %d, P% :%d', state.t1, heaters[1].power);
                } else {
                    logger.debug('T C : %d, h1 :%s', state.t1, state.h1);
                }

                thempFinish();
            })();
        } catch (ex) {
            logger.error(ex);
        }
    }
    // main heat
    function createStatus() {
        return {
            fecha: new Date(),
            ticks: state.ticks,
            temperatures: [state.t1, state.t5, state.t4, state.t3, state.t2, state.t6],
            heaters: [state.h1, 'off', 'off', state.h3],
            boiler1: state.wh1,
            P: state.P
        };
    }
}

exports.thermoReg = thermoReg;
exports.setConfStateCounts = setConfStateCounts;
function setConfStateCounts(conf, _state, _cnt) {
    casaConfig = conf;
    state = _state;
    counts = _cnt;
    if (!thermoRegStarted) {
        thermoReg();
        setInterval(thermoReg, 15 * 1000);
        thermoRegStarted = true;
        logger.debug('thermoRegStarted', thermoRegStarted);
    }
}
function thermoReg() {
    casaConfig = piutils.config();
    //logger.debug('heatMode: %d',casaConfig.heatMode);
    zone = piutils.updateZone();
    var isDay = zone == 0;
    //logger.debug("is day:%d",isDay);
    var room = casaConfig.rooms[0];
    var hysteresis = casaConfig.hysteresis;
    aver = parseFloat(isDay ? room.dayThemp.aver : room.nightThemp.aver);
    var halfHyst = casaConfig.hysteresis / 2;
    //histMin = aver - halfHyst;
    //histMax = aver + halfHyst;
    histMin = aver - hysteresis;
    histMax = aver;

    //logger.debug("before getThemp");
    getThemp();
}

// socketClient.client.emit('msg:state',createStatus());
//console.log('emit');

//# sourceMappingURL=themp.js.map