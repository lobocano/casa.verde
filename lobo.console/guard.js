/**
 * Created by pablo on 18.10.14.
 */
'use strict';

var piutils = require('./piutils');
var winston = require('winston');
var Gpio = require('onoff').Gpio;
var logger = new winston.Logger({
    transports: [new winston.transports.Console({ level: 'debug' }), new winston.transports.File({
        filename: '/home/pi/casaverde/logs/guard.log',
        level: 'debug',
        json: false,
        maxsize: 65536,
        maxFiles: 5 })]
});
var moment = require('moment');
var sockets = require('./sockets');
var socketClient = sockets.getInstance();

var conf;
var doorSensor;
var counter = 0,
    prevCounter = 0;
var lastRiseTime = moment();

var guardObject = {
    doors: [{ state: 0, lastOpenTime: moment(), lastCloseTime: moment() }]
};
exports.initGuard = function (_conf) {
    conf = _conf;
    piutils.readGuard(function (err, _guard) {
        if (err) {
            logger.error('Error read guard: %s', err);
        } else {
            guardObject = _guard;
        }
        doorSensor = new Gpio(23, 'in', 'both', { debounceTimeout: 300 });
        doorSensor.watch(doorWatch);
        logger.info('Guard init');
    });

    /*doorSensor.watch(function(err, value){
        if (err) {
            logger.error('Error watch door: %s', err);
         }
        else{
            counter++;
            logger.debug('Door opened:%d', counter);
        }
    });*/
};

function doorWatch(err, value) {
    if (err) {
        logger.error('Error watch door: %s', err);
    } else {
        var current = moment();
        var diff = current.diff(lastRiseTime);
        var readVal;
        doorSensor.read(function (er, v) {
            if (er) logger.error('read error ', er);else {
                var finGuard = function finGuard() {
                    logger.debug('Door:%d val:%d - %d', counter, value, readVal);
                    piutils.writeGuard(guardObject, function (err) {
                        if (err) logger.error('Error write guard file ', err);
                    });
                };

                readVal = v;
                lastRiseTime = current;
                counter++;
                guardObject.doors[0].state = readVal;
                if (sockets.connection) {
                    sockets.io.emit('event:guard', { door: guardObject.doors[0].state });
                    //console.log('emit');
                }
                if (socketClient.connected && !socketClient.isRoomEmpty) {
                    socketClient.client.emit('msg:guard', { door: guardObject.doors[0].state });
                    logger.debug('emit msg:guard');
                }
                if (readVal == 1) {
                    guardObject.doors[0].lastOpenTime = current;
                    guardObject.doors[0].lastCloseTime = null;
                    if (piutils.config().guardMode == 1) {
                        //logger.debug('alarm');
                        if (piutils.config().smsalarm == 1) {
                            piutils.sendSms('Alarm', function (err) {
                                if (err) logger.error(err.description);else logger.debug('sms done');
                            });
                        }
                        if (piutils.config().eMailAlarm == 1) {
                            piutils.sendMail('Door opened!', 'Alarm', function (err) {
                                if (err) logger.error(err);else logger.debug('email done');
                            });
                        }
                    }
                } else {
                    guardObject.doors[0].lastCloseTime = current;
                }
                finGuard();
            }
        });
    }
}

//# sourceMappingURL=guard.js.map

//# sourceMappingURL=guard.js.map

//# sourceMappingURL=guard.js.map

//# sourceMappingURL=guard.js.map