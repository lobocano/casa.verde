/**
 * Created by pablo on 07.07.15.
 */
import app  from './app';
import sockets from './sockets';
import http from 'http';
import opt from 'optimist';

if(opt.argv.sam){
    console.log('Sam logic');
}
else if(opt.argv.queso) {
    console.log('Queso logic');
}

var port = normalizePort(process.env.PORT || '3003');
app.set('port', port);

var server = http.createServer(app);

sockets.create(server);
server.listen(port);
server.on('listening', onListening);

function normalizePort(val) {
    var port = parseInt(val, 10);

    if (isNaN(port)) {
        // named pipe
        return val;
    }

    if (port >= 0) {
        // port number
        return port;
    }

    return false;
}

function onListening() {
    var addr = server.address();
    var bind = typeof addr === 'string'
        ? 'pipe ' + addr
        : 'port ' + addr.port;
    console.log('Listening on ' + bind);
}