'use strict';

/**
 * @ngdoc function
 * @name skApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the skApp
 */
angular.module('skApp')
  .controller('MainCtrl',['$scope','ngAudio', function ($scope, ngAudio) {
      console.log('MainCtrl');
        $scope.date = new Date();
        //$scope.ticks = 0;
        $scope.state = {t1:"19.0",h1:"on",h3:"on", t5:"7.6", t3:"3.9", t4:"16.1", t2:-2.4};
        $scope.guard = {door:0};
        $scope.doorOpen = ngAudio.load("images/dooropen.mp3");
        $scope.doorClose = ngAudio.load("images/doorclose.mp3");
        //var inputElement = $('#audio1');
        //console.log(inputElement);
        //inputElement.play();
        //$scope.sound.play();
        var socket = io();
        socket.on('event:state', function(state){

            $scope.date = state.fecha;
            //$scope.ticks = state.ticks;
            $scope.state = state.state;
            //console.log($scope.date);
            $scope.$apply();
        });
        socket.on('event:guard', function(guard){
            $scope.guard = guard;
            $scope.$apply();
            if($scope.guard.door == 1)  $scope.doorOpen.play();
            else $scope.doorClose.play();
        });

}]);
