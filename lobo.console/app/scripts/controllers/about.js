'use strict';

/**
 * @ngdoc function
 * @name skApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the skApp
 */
angular.module('skApp')
  .controller('AboutCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
