'use strict';

/**
 * @ngdoc overview
 * @name skApp
 * @description
 * # skApp
 *
 * Main module of the application.
 */
angular
  .module('skApp', [
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngAudio'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: '/partials/main.jade',
        controller: 'MainCtrl'
      })
      .when('/about', {
        templateUrl: '/partials/about.jade',
        controller: 'AboutCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
